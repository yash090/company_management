const expencesModel = require('../models/expences.model');

const JOI = require('joi');
const schema = JOI.object().keys({
    aid: JOI.number().integer(),
    expence_employees_id: JOI.number().integer(),
    expence_amount: JOI.string().trim(),
    expence_reason: JOI.string().trim()
})

exports.getExpences = (req, res)=> {
    //console.log('here all expences list');
    expencesModel.getExpences((err, exp) =>{
        console.log('We are here');
        if(err)
        res.send(err);
        console.log('Expences', exp);
        res.send(exp)
    })
}

// get expences by ID
exports.getExpencesByID = (req, res)=>{
    //console.log('get expences by id');
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    expencesModel.getExpencesByID(req.params.id, (err, exp)=>{
        if(err)
        res.send(err);
        console.log('Employee expences',exp);
        res.send(exp);
    })
}}


// create new expences record
exports.createNewExpences = (req, res) =>{
    const expencesReqData = new expencesModel(req.body);
    console.log('expencesReqData', expencesReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        expencesModel.createNewExpences(expencesReqData, (err, exp)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'expences Created Successfully', data: exp.insertId})
        })
    }
}


// update expences
exports.updateExpences = (req, res)=>{
    const expencesReqData = new expencesModel(req.body);
    console.log('expencesReqData update', expencesReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        expencesModel.updateExpences(req.params.id, expencesReqData, (err, exp)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'expences updated Successfully'})
        })
    }
}


// delete expences
exports.deleteExpences = (req, res)=>{
    const val = req.params.id
    const result = parseInt(val)
    if(!result){
        res.json({status:false,message:"Id should be an integer"})
    }else{
    expencesModel.deleteExpences(req.params.id, (err, exp)=>{
        if(err)
        res.send(err);
        res.json({success:true, message: 'expences deleted successfully!'});
    })
}}